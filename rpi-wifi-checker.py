#! /usr/bin/python3

""" Raspberry Pi connection checker

Summary:
    This script was written for Raspbian to check the Pi's connection to the router and
    if the connection has been lost for more than the previously set limit then the device is rebooted.
    The aim is only to ensure connection to the local network - internet connection is not in the scope.

Use case:
    In some cases when connection to the router is lost for a longer time the Pi shuts down the wifi module
    and will not restart it later leaving it unreachable until manual restart. This is a crude but efficient workaround.

Installation:
    Run it with SU privileges after each reboot, eg. add it to the root crontab with @reboot
"""

import subprocess
import logging
import time
import os

connection_check_interval_sec = 5
connection_last_checked_at = -100
connection_lost_limit_sec = 5 * 60
connection_lost_at = None

logger = logging.getLogger('rpi_wifi_checker')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh = logging.FileHandler('./rpi_wifi_checker.log')
fh.setLevel(logging.INFO)
fh.setFormatter(formatter)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)

while True:
    sub_process = subprocess.Popen(['iwgetid'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    current_time = time.monotonic()
    check_is_due = current_time - connection_last_checked_at >= connection_check_interval_sec
    if check_is_due:
        logger.debug('Running check')
        try:
            connection_last_checked_at = current_time
            # Check for existing router connection
            access_point = subprocess.check_output(('grep', 'ESSID'), stdin=sub_process.stdout)
            # Reset timer on reconnection
            if connection_lost_at is not None:
                logger.info('Connected to: {}'.format(access_point.decode()))
                connection_lost_at = None
        except subprocess.CalledProcessError as error:
            # Handle connection lost
            if connection_lost_at is None:
                connection_lost_at = current_time
                logger.info('Warning not connected to the local network!')
            elif current_time - connection_lost_at >= connection_lost_limit_sec:
                logger.info('Disconnected for more than {}sec. Rebooting device.'.format(connection_lost_limit_sec))
                os.system('sudo shutdown -r now')
